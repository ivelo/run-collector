import requests
import sys
import json

api_host = 'http://10.128.124.243:8003'

rundb_host = 'http://rundb-internal.lbdaq.cern.ch/api/run/'
keylist = ['runid', 'partitionid', 'runtype', 'partitionname','destination', 
           'beamenergy', 'fillid', 'startlumi', 'endlumi', 'activity', 'LHCState',
           'starttime', 'endtime', 'avLumi', 'avMu', 'veloOpening', 'veloPosition',
           'prev_runid', 'next_runid']

def _get_rundb(runnumber):
    session = requests.Session()
    session.trust_env = False
    content = session.get(
        "{}{}".format(rundb_host, runnumber),
        verify = False,
    )
    content.raise_for_status()
    return content.json()

def _get_latest_ivelo(partitionname):
    session = requests.Session()
    session.trust_env = False
    content = session.get(
        "{}{}".format(api_host, '/api/run/latest'),
        verify = False,
        params = {'partitionname': partitionname}
    )
    content.raise_for_status()
    return content.json()

def _post_ivelo(apath, data):
    session = requests.Session()
    session.trust_env = False
    content = session.post(
        "{}{}".format(api_host, '/api/' + apath),
        data = json.dumps(data),
        verify = False,
    )
    content.raise_for_status()
    return content.json()

#call only when installing new instance of the software
def _add_new_partition(partition):
    try:
        latest = _get_latest_ivelo(partition)
    except requests.exceptions.HTTPError:
        pass
    
    if latest is not None:
        print('... partition already exists, terminating...')
        sys.exit(0)
    
    try:
        latest = {'LHCb': 252000, 'VELO': 252028}[partition]
    except KeyError:
        print('... ... partition name not in the predefined list.\n')
        raise KeyError
    
    print(f'... I will collect runs from {latest} for partition {partition}.')


def collect(partition):
    try:
        latest = _get_latest_ivelo(partition)
    except requests.exceptions.HTTPError:
        print('... API Internal Server Error\n' +
              '... ... please use appropriate naming LHCb/VELO\n' +
              '... ... please check if the db server is ON (if OFF, run the server)\n' +
              '... ... if still does not work, contact the developer.\n')
        sys.exit(0)

    latest_runid = latest['runid']
    while True:
        runinfo = _get_rundb(latest_runid)
        data = {key: runinfo[key] for key in runinfo if key in keylist}
        _post_ivelo('run', data)
        #print(f'... adding run {latest_runid} for partition {partition}')

        if 'next_runid' in runinfo.keys():
            latest_runid = runinfo['next_runid']
        else:
            #print('... all runs added!')
            break

if __name__ == '__main__':
    collect(sys.argv[1])
